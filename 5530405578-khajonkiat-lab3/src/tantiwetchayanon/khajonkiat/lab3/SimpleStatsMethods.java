package tantiwetchayanon.khajonkiat.lab3;

import java.util.Arrays;

public class SimpleStatsMethods {
	static int numGPAs;
	static double[] nums;
	static double sum;
	public static void main(String[] args) {
		if(args.length < 2){
			System.out.println("Usage:<SimpleStats> <numGPAs> <GPA>..");
			System.exit(0);
		}
		numGPAs = Integer.parseInt(args[0]);
		nums = new double[numGPAs];
		acceptInputs(args);
		displayStart(nums);
	}
	public static void acceptInputs(String[] args) {
		System.out.println("For the input GPAs: ");
		for (int i=0; i<numGPAs; i++)
		{
			nums[i] = Double.parseDouble(args[i+1]);
			System.out.print( args[i+1] + " " );
			sum = sum + nums[i];
		}
		
	}
	private static void displayStart(double[] nums2) {
		Arrays.sort(nums);
		System.out.println();
		System.out.println("star: ");
		System.out.println("Avg GPA is " + (sum/numGPAs) );
		System.out.println("Min GPA is " + nums[0]);
		System.out.println("Max GPA is " + nums[numGPAs-1]);
		
	}
}
