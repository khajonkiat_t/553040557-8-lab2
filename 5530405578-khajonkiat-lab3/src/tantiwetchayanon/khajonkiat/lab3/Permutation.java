package tantiwetchayanon.khajonkiat.lab3;

public class Permutation {
	public static void main (String[] args){
		if (args.length != 2) {
			System.err.println("Usage:Permutation <n> <k>");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n, k);
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);
	}

	public static long permute(int n, int k) {
		long ans1 = 1 , ans2 = 1 , ans3 = 1;
		for (int i=1; i <= n; i++){
			ans1 = ans1 * i;
			if(i <= n-k)
			{
				ans2 = ans2 * i;
			}
			
			if(i >= k)
			{
				ans3 *= i;
			}
		}
		System.out.println(n + "! = " + ans1);
		System.out.println("(" + n + "-" + k + ")! = " + ans2);
		return ans3;
	}

}
